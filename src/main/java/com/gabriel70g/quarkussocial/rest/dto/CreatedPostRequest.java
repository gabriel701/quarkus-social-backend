package com.gabriel70g.quarkussocial.rest.dto;

import lombok.Data;

@Data
public class CreatedPostRequest {
    private String text;
}
