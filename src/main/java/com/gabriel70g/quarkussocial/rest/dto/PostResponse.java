package com.gabriel70g.quarkussocial.rest.dto;

import com.gabriel70g.quarkussocial.rest.domain.model.Post;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PostResponse {
    private String text;
    private LocalDateTime dateTime;

    public static  PostResponse fromEntity (Post post){
        var response = new PostResponse();
        response.setText(post.getText());
        response.setDateTime(post.getDataTime());
        return response;
    }
}
