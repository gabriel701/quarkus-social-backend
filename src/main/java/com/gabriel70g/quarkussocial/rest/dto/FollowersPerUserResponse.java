package com.gabriel70g.quarkussocial.rest.dto;

import lombok.Data;

import java.util.List;

@Data
public class FollowersPerUserResponse {
    private int followersCount;
    private List<FollowerResponse> content;

}
