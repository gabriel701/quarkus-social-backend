package com.gabriel70g.quarkussocial.rest;

import com.gabriel70g.quarkussocial.rest.domain.model.Post;
import com.gabriel70g.quarkussocial.rest.domain.model.User;
import com.gabriel70g.quarkussocial.rest.domain.repository.FollowerRepository;
import com.gabriel70g.quarkussocial.rest.domain.repository.PostRepository;
import com.gabriel70g.quarkussocial.rest.domain.repository.UserRepository;
import com.gabriel70g.quarkussocial.rest.dto.CreatedPostRequest;
import com.gabriel70g.quarkussocial.rest.dto.PostResponse;
import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Sort;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.stream.Collectors;

@Path("/users/{userId}/posts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PostResources {

    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final FollowerRepository followerRepository;



    @Inject
    public  PostResources(UserRepository userRepository, PostRepository postRepository, FollowerRepository followerRepository) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.followerRepository = followerRepository;
    }


    @POST
    @Transactional
    public Response savePost(@PathParam("userId") Long userId,
                             CreatedPostRequest request){
        User user = userRepository.findById(userId);

        if (user == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        Post post = new Post();
        post.setText(request.getText());
        post.setUser(user);

        postRepository.persist(post);

        return Response.status(Response.Status.CREATED).build();
    }

    @GET
    public Response listPost(
            @PathParam("userId") Long userId,
            @HeaderParam("followerId") Long followerId
            ){
        User user = userRepository.findById(userId);

        if (user == null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        if (followerId == null){
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("You forgot the header")
                    .build();
        }

        User follower = userRepository.findById(followerId);

        if (follower == null){
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("Nonexistent followerId")
                    .build();
        }

        boolean follows = followerRepository.follows(follower, user);

        if (!follows){
            return Response.status(Response.Status.FORBIDDEN)
                    .entity("You can`t see these posts")
                    .build();
        }

        PanacheQuery<Post> query = postRepository.find("user"
                , Sort.by("dataTime", Sort.Direction.Descending)
                , user);

        var list = query.list();

        var postResponseList = list.stream()
                .map(PostResponse::fromEntity)
                .collect(Collectors.toList());

        return Response.ok(postResponseList).build();
    }

}
