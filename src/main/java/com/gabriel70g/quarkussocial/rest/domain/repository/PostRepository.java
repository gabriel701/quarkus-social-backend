package com.gabriel70g.quarkussocial.rest.domain.repository;

import com.gabriel70g.quarkussocial.rest.domain.model.Post;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PostRepository implements PanacheRepository<Post> {
}
