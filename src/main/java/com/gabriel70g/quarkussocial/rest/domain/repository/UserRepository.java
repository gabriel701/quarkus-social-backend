package com.gabriel70g.quarkussocial.rest.domain.repository;

import com.gabriel70g.quarkussocial.rest.domain.model.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {
}
