package com.gabriel70g.quarkussocial.rest;

import com.gabriel70g.quarkussocial.rest.domain.model.Follower;
import com.gabriel70g.quarkussocial.rest.domain.model.User;
import com.gabriel70g.quarkussocial.rest.domain.repository.FollowerRepository;
import com.gabriel70g.quarkussocial.rest.domain.repository.UserRepository;
import com.gabriel70g.quarkussocial.rest.dto.FollowerRequest;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;

import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.core.Response;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;
@QuarkusTest
@TestHTTPEndpoint(FollowerResource.class)
class FollowerResourceTest {
    @Inject
    FollowerRepository followerRepository;
    @Inject
    UserRepository userRepository;

    Long userId;
    Long followerId;

    @BeforeEach
    @Transactional
    void setUp() {
        var user = new User();
        user.setName("Gabriel");
        user.setAge(30);
        userRepository.persist(user);
        userId = user.getId();

        var follower = new User();
        follower.setName("Gabriel");
        follower.setAge(60);
        userRepository.persist(follower);
        followerId = follower.getId();

        var followerEntity = new Follower();
        followerEntity.setUser(user);
        followerEntity.setFollower(follower);

        followerRepository.persist(followerEntity);
    }

    @Test
    @DisplayName("should return 409 when followerId is equal to userId")
    public void SameUserAsFollowerTest() {
        var body = new FollowerRequest();
        body.setFollowerId(userId);

        given()
                .contentType(ContentType.JSON)
                .body(JsonbBuilder.create().toJson(body))
                .pathParams("userId" , userId)
                .when()
                .put()
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(Matchers.is("You can't follow yourself"));
    }

    @Test
    @DisplayName("should return 404 userId not found")
    public void SameUserNotFoundTest() {
        var body = new FollowerRequest();
        body.setFollowerId(followerId+100);

        given()
                .contentType(ContentType.JSON)
                .body(JsonbBuilder.create().toJson(body))
                .pathParams("userId" , userId+100)
                .when()
                .put()
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @DisplayName("should return 404 userId not found")
    public void userNotFoundTest() {
        var body = new FollowerRequest();
        body.setFollowerId(userId);

        var userInexistente = userId+100;

        given()
                .contentType(ContentType.JSON)
                .body(JsonbBuilder.create().toJson(body))
                .pathParams("userId" , userInexistente)
                .when()
                .put()
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @DisplayName("should follow a user")
    public void followUserTest() {

        var body = new FollowerRequest();
        body.setFollowerId(followerId);

          given()
                .contentType(ContentType.JSON)
                .body(JsonbBuilder.create().toJson(body))
                .pathParams("userId" , userId)
                .when()
                .put()
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

    @Test
    @DisplayName("should return 404 on list user followers and id doesn't exist")
    public void listUserFollowersNotFoundTest() {

        var body = new FollowerRequest();
        body.setFollowerId(userId);

        var userInexistente = userId+99;

        given()
                .contentType(ContentType.JSON)
                .pathParams("userId" , userInexistente)
                .when()
                .get()
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }


    @Test
    @DisplayName("should list a user's followers")
    public void listUserFollowersTest() {


        var response = given()
                .contentType(ContentType.JSON)
                .pathParams("userId" , userId)
                .when()
                .get()
                .then()
                .extract().response();

        var followerCount = response.jsonPath().get("followersCount");
        var followerContent = response.jsonPath().getList("content");
        assertEquals(Response.Status.OK.getStatusCode(), response.statusCode());
        assertEquals(1, followerCount);
        assertEquals(1, followerContent.size());
    }

    @Test
    @DisplayName("should return on unfollow a user and id doesn't exist")
    public void useNotFoundedUnFollowingAUserTest() {

        var body = new FollowerRequest();
        body.setFollowerId(userId);

        var userInexistente = userId+99;

        given()
                .contentType(ContentType.JSON)
                .pathParams("userId" , userInexistente)
                .queryParam("followerId" , followerId)
                .when()
                .delete()
                .then()
                .statusCode(Response.Status.NOT_FOUND.getStatusCode());
    }

    @Test
    @DisplayName("should unfollow an user")
    public void unfollowAndUserTest() {

        var body = new FollowerRequest();
        body.setFollowerId(userId);

        given()
                .contentType(ContentType.JSON)
                .pathParams("userId" , userId)
                .queryParam("followerId" , followerId)
                .when()
                .delete()
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());
    }

}