package com.gabriel70g.quarkussocial.rest;

import com.gabriel70g.quarkussocial.rest.domain.model.Follower;
import com.gabriel70g.quarkussocial.rest.domain.model.Post;
import com.gabriel70g.quarkussocial.rest.domain.model.User;
import com.gabriel70g.quarkussocial.rest.domain.repository.FollowerRepository;
import com.gabriel70g.quarkussocial.rest.domain.repository.PostRepository;
import com.gabriel70g.quarkussocial.rest.domain.repository.UserRepository;
import com.gabriel70g.quarkussocial.rest.dto.CreatedPostRequest;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import jakarta.json.bind.JsonbBuilder;
import jakarta.transaction.Transactional;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
@TestHTTPEndpoint(PostResources.class)
class PostResourcesTest {
    @Inject
    UserRepository userRepository;

    @Inject
    FollowerRepository  followerRepository;

    @Inject
    PostRepository  postRepository;

    Long userId;
    Long userNotFollowerId;
    Long userFollowerId;


    @BeforeEach
    @Transactional
    public void Setup(){
        var user = new User();
        user.setName("Gabriel");
        user.setAge(30);
        userRepository.persist(user);
        userId = user.getId();

        var userNotFollower = new User();
        userNotFollower.setName("segundo");
        userNotFollower.setAge(30);
        userRepository.persist(userNotFollower);
        userNotFollowerId = userNotFollower.getId();

        var userFollower = new User();
        userFollower.setName("tercero");
        userFollower.setAge(30);
        userRepository.persist(userFollower);
        userFollowerId = userFollower.getId();

        Follower follower = new Follower();
        follower.setUser(user);
        follower.setFollower(userFollower);
        followerRepository.persist(follower);

        Post post = new Post();
        post.setUser(user);
        post.setText("This is a new post");
        postRepository.persist(post);
    }


    @Test
    @DisplayName("should create a new post")
    @Transactional
    public void shouldCreateAPost(){
        var postRequest = new CreatedPostRequest();

        postRequest.setText("This is a new post");

        given()
                .contentType("application/json")
                .body(JsonbBuilder.create().toJson(postRequest))
                .pathParams("userId", userId)
                .when()
                .post()
                .then()
                .statusCode(201);
    }

    @Test
    @DisplayName("should create a new post existent user")
    @Transactional
    public void shouldCreateAPostInexistentUser(){
        var postRequest = new CreatedPostRequest();

        postRequest.setText("This is a new post");

        var inexistentUserId = userId + 10;

        given()
                .contentType("application/json")
                .body(JsonbBuilder.create().toJson(postRequest))
                .pathParams("userId", inexistentUserId)
                .when()
                .post()
                .then()
                .statusCode(404);
    }

    @Test
    @DisplayName("list post user not found")
    public void listPostUserNotFoundTest(){
        var nonexistentUserId = userId + 100;

        given()
                .pathParams("userId", nonexistentUserId)
                .when()
                .get()
                .then()
                .statusCode(404);
        }

    @Test
    @DisplayName("list post follow  header not send")
    public void listPostFollowHeaderNotSendTest(){
        given()
                .pathParams("userId", userId)
                .when()
                .get()
                .then()
                .statusCode(400)
                .body(Matchers.is("You forgot the header"));
    }

    @Test
    @DisplayName("list post follow doesn't exists")
    public void listPostNotAFollowNotExistsTest(){

        var nonexistentUserId = userId + 100;

        given()
                .pathParams("userId", userId)
                .header("followerId", nonexistentUserId)
                .when()
                .get()
                .then()
                .statusCode(400)
                .body(Matchers.is("Nonexistent followerId"));

    }

    @Test
    @DisplayName("list return 403 when follower isn't a follower")
    public void listPostNotAFollowTest(){


        given()
                .pathParams("userId", userId)
                .header("followerId", userNotFollowerId)
                .when()
                .get()
                .then()
                .statusCode(403)
                .body(Matchers.is("You can`t see these posts"));
    }

    @Test
    @DisplayName("list return posts")
    public void listPostTest(){
        given()
                .pathParams("userId", userId)
                .header("followerId", userFollowerId)
                .when()
                .get()
                .then()
                .statusCode(200)
                .body("size()",  Matchers.is( Matchers.is(1)));

    }
}