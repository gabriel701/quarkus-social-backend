package com.gabriel70g.quarkussocial.rest;

import com.gabriel70g.quarkussocial.rest.dto.CreateUserRequest;
import com.gabriel70g.quarkussocial.rest.dto.ResponseError;
import io.quarkus.test.common.http.TestHTTPResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.json.bind.JsonbBuilder;
import org.junit.jupiter.api.*;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@QuarkusTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserResourcesTest {

    @TestHTTPResource("/users")
    String baseUri;



    @Test
    @DisplayName("Create User Test")
    @Order(1)
    public void createUserTest() {
        var user = new CreateUserRequest();
        user.setName("Gabriel");
        user.setAge(53);

        var response =
                given()
                        .contentType(ContentType.JSON)
                        .body(JsonbBuilder.create().toJson(user))
                        .when()
                        .post(baseUri)
                        .then()
                        .extract().response();

        assertEquals(201, response.statusCode());
        assertNotNull(response.jsonPath().getString("id"));
    }


    @Test
    @DisplayName("should return 422 when create user with invalid data")
    @Order(2)
    public void createUserValidatorErrorTest() {
        var user = new CreateUserRequest();
        user.setName(null);
        user.setAge(null);
        var response =
                given()
                        .contentType(ContentType.JSON)
                        .body(JsonbBuilder.create().toJson(user))
                        .when()
                        .post(baseUri)
                        .then()
                        .extract().response();

        assertEquals(ResponseError.UNPROCESABLE_ENTITY_STATUS, response.statusCode());
        assertEquals("Validation Error", response.jsonPath().getString("message"));

        List<Map<String, String>> errors = response.jsonPath().getList("errors");
        assertNotNull(errors.get(0).get("message"));
        assertNotNull(errors.get(1).get("message"));
    }

    @Test
    @DisplayName("List All Users Test")
    @Order(3)
    public void ListAllUsersTest(){

    given()
            .contentType(ContentType.JSON)
            .when()
            .get(baseUri)
            .then()
            .statusCode(200);
    }
}