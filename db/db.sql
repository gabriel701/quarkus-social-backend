create table "quarkus-social".public.users(

id bigserial not null primary key,
name varchar(100) not null,
age integer not null
)

create table "quarkus-social".public.post(
id bigserial not null primary key,
post_text varchar(150) not null,
dateTime timestamp not null,
user_id bigint not null references users(id)
)

create table "quarkus-social".public.followers(
id bigserial not null primary key,
user_id bigint not null references users(id),
follower_id bigint not null references users(id)
)

CREATE ROLE "quarkus-social" NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN NOREPLICATION NOBYPASSRLS PASSWORD '123456';
GRANT ALL ON TABLE public.users TO "quarkus-social";
GRANT ALL ON SCHEMA public TO "quarkus-social";
